import Component from '@glimmer/component';
import { arg } from 'ember-arg-types';

export default class Locator extends Component {
  // Component Input Properties
  // --------------------------
  @arg specialist;
}
