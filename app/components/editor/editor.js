import Component from '@glimmer/component';
import { action, set } from '@ember/object';
import { inject } from '@ember/service';
import { arg } from 'ember-arg-types';

/**
 * Editor component. Displays all deck cards and provides functionality
 * to replace the images for the ones you desire. It is connected to the
 * editor manager for deck submission and new deck creation.
 */
export default class Editor extends Component {
  @inject qrManager;
  @inject nftManager;
  @inject notifications;

  dom = null
  cids = null
  qrCode = null
  deckUrl = null
  submittingDeck = false

  // Component Input Properties
  // --------------------------
  @arg deck;

  /**
   * Returns list of uploaded deck images.
   */
  newIllustrationFiles() {
    const inputs = this.dom.querySelectorAll("input[id^='file-input-']")
    return Array.from(inputs).map((i) => i?.files[0])
  }

  /**
   * Submit file to NFT Storage
   * @param file
   * @return {*}
   */
  submitFile(file) {
    return this.nftManager.submitImage(file)
  }

  /**
   * Only when all deck designs are replaced editors can submit their decks
   * @return {boolean}
   */
  canSubmitNewDeck() {
    return !this.newIllustrationFiles().some((f) => f === undefined || f === null)
  }

  @action
  async loadDom(element) {
    set(this, 'dom', element);
  }

  @action
  async submitNewDeck() {
    set(this, 'submittingDeck', true)

    if (!this.canSubmitNewDeck()) {
      this.notifications
        .error('Deck could not be submitted. Be sure all card illustrations are replaced', { autoClear: true })
    } else {
      const responses = []
      for(const file of this.newIllustrationFiles()) {
        if(file) {
          try {
            const result = await this.submitFile(file)
            responses.push(result)
          } catch(e) {
            set(this, 'submittingDeck', false)
            this.notifications.error('Error while submitting card illustrations to cloud storage', { autoClear: true })
            break
          }
        }
      }
      const cids = this.nftManager.generateCids(responses)
      const deckUrl = this.nftManager.generateDeckUrl(responses)
      const qrCode = await this.qrManager.generateQrImageUrl(deckUrl)
      set(this, 'cids', cids)
      set(this, 'qrCode', qrCode)
      set(this, 'deckUrl', deckUrl)
    }
  }

  @action
  copyToClipboard(cid) {
    const el = document.createElement('textarea');
    el.value = cid;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.notifications.success('CID copied in clipboard', { autoClear: true })
  }
}
