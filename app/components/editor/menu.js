import Component from '@glimmer/component';
import { action } from '@ember/object';
import { arg } from 'ember-arg-types';

/**
 * This is the menu displayed inside the modal defined under
 * the tutorial navbar to introduce users the game editor and
 * allow them to update the current Deck.
 */
export default class Menu extends Component {
  // Component Input Properties
  // --------------------------
  @arg deck;
  @arg modal;

  @action
  closeModal() {
    this.modal.hide();
  }
}
