import Component from '@glimmer/component';
import { action, set } from '@ember/object';
import { arg } from 'ember-arg-types';

/**
 * Displays deck cards and provides an option to replace the
 * current card image by a new one.
 */
export default class CardInput extends Component {
  // Component Input Properties
  // --------------------------
  @arg card;
  @arg index;

  dom = null;
  newCards = [];
  cardLoaded = false;

  submittedFileUrl(event) {
    return event?.target?.files[0];
  }

  updateCardImage(newImageUrl) {
    this.dom.querySelector('.mind-front-card').style.background = `url('${newImageUrl}') no-repeat center`
  }

  @action
  async loadDom(element) {
    set(this, 'dom', element);
  }

  /**
   * Selects new image for selected card. Replaces current card
   * illustration by selected one and updates the new deck information.
   * @param e
   */
  @action selectNewCardImage(e) {
    const imageUrl = this.submittedFileUrl(e)
    if (!imageUrl) return
    this.updateCardImage(URL.createObjectURL(imageUrl))
    set(this, 'cardLoaded', true)
  }

  /**
   * Replaces input field image by default card image and clears input field.
   */
  @action restoreCardImage() {
    set(this, 'cardLoaded', false)
    this.dom.querySelector('input').value = null
    this.updateCardImage(`${this.card.image}`)
  }
}
