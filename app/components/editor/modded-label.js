import Component from '@glimmer/component';
import { action } from '@ember/object';
import { arg } from 'ember-arg-types';

export default class ModdedLabel extends Component {
  modal = null;

  // Component Input Properties
  // --------------------------
  @arg deck;

  @action
  showModal() {
    this.modal.show();
  }

  @action
  closeModal() {
    this.modal.hide();
  }
}
