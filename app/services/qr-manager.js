/* eslint-disable no-undef */
import Service from '@ember/service';

/**
 * Logic to generate QR codes using QR code API
 */
export default class QrManager extends Service {
  _nftAjaxConfiguration(qrContent) {
    return {
      async: true,
      method: "GET",
      crossDomain: true,
      url: this.generateQrImageUrl(qrContent)
    }
  }

  /**
   * Generates URL to QR API endpoint where PNG image is generated
   * @param qrContent
   * @return {string}
   */
  generateQrImageUrl(qrContent) {
    return `https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${qrContent}`
  }

  /**
   * Given a string generates a QR image which contains the code information.
   * @param {string} qrContent
   */
  generateQrImage(qrContent) {
    return $.ajax(this._nftAjaxConfiguration(qrContent))
  }
}
