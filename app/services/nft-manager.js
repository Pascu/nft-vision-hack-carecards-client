/* eslint-disable no-undef */
import Service from '@ember/service';

/**
 * Logic to store and retrieve images from NFT-Store service
 */
export default class NftManager extends Service {
  _nftAjaxConfiguration() {
    return {
      async: true, crossDomain: true, url: "https://api.nft.storage/upload", method: "POST",
      headers: {
        authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweDdjOEIyQzFCYTA0RjljQzcyZDk1ZThkODMxRkMxOUM3RDJBRDViYkMiLCJpc3MiOiJuZnQtc3RvcmFnZSIsImlhdCI6MTYyNTExNjY3NzUwNywibmFtZSI6IkNhcmVDYXJkc0hhY2thdGhvbktleSJ9.poIwMHC3QSG-1FcAZbfNqJWOBRUJr00ElN_GUNSrBGw",
      }
    }
  }

  /**
   * Given a NFT Store image file submission response extracts the corresponding submitted
   * image CID
   * @param response
   * @private
   */
  _cid(response) {
    return response?.value?.cid
  }

  /**
   * Was the submitted image file successfully uploaded?
   * @param response
   * @return {*}
   * @private
   */
  _valid(response) {
    return response.ok
  }

  /**
   * Given a collection of CIDs it generates an URL that concats all CIDs
   * in order to allow players load deck with submitted images.
   * @param cids
   * @return {string}
   * @private
   */
  _deckUrl(cids) {
    if(cids) return `${ window.location.origin }?deck_cids=${ cids.map((cid) => `${cid}`) }`
  }

  /**
   * Given an image file from a file input it submits the file to the NFT Storage via HTTP request
   * to the API.
   * @param imageFile
   * @return {*}
   */
  submitImage(imageFile) {
    return $.ajax(Object.assign(this._nftAjaxConfiguration(), { data: imageFile, processData: false }))
  }

  generateCids(nftSubmissionResponses) {
    if(nftSubmissionResponses.some((r) => !this._valid(r))) return null
    return nftSubmissionResponses.map((r) => this._cid(r))
  }

  /**
   * Given a collection of API responses from NFT Storage on file image submission the function
   * processes all responses information to generate an URL that players can use to load the
   * game with the submitted illustrations
   * @param nftSubmissionResponses
   */
  generateDeckUrl(nftSubmissionResponses) {
    return this._deckUrl(this.generateCids(nftSubmissionResponses))
  }

  /**
   * Given a collection of CIDs returns a new collection with the URL of the image related to each CID
   * @param cids
   */
  generateImagesUrl(cids) {
    return cids.map((cid) => `https://${cid}.ipfs.dweb.link`)
  }
}
