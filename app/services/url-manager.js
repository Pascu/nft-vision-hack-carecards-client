import Service from '@ember/service';

/**
 * Service to gather basic information about how and where the
 * application is deployed (standalone app, iFrame, etc).
 */
export default class UrlManager extends Service {
  extractParameter(parameter) {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    return urlParams.get(parameter);
  }
}
