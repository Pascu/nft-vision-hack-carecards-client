'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'ember-cards',
    environment,
    rootURL: '',
    locationType: 'hash',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      host: 'http://localhost:3000',
      template: 'default'
    },

    // Default Game Deck
    deck: 'coronaAnxiety',

    // NFT Storage Key
    nftStorageKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweDdjOEIyQzFCYTA0RjljQzcyZDk1ZThkODMxRkMxOUM3RDJBRDViYkMiLCJpc3MiOiJuZnQtc3RvcmFnZSIsImlhdCI6MTYyNTExNjY3NzUwNywibmFtZSI6IkNhcmVDYXJkc0hhY2thdGhvbktleSJ9.poIwMHC3QSG-1FcAZbfNqJWOBRUJr00ElN_GUNSrBGw'
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }

  return ENV;
};
