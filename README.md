# NFT Hack Vision - CareCards Deck Illustrator
This is the project repository of the CareCards team for the submission of the Illustration Deck editor for the NFT Hack Vision competition.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://vimeo.com/manage/videos/590607848" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Project demo video: https://vimeo.com/manage/videos/590607848

Project demo live demo link:
http://nft-vision-hack-carecards.s3-website.eu-central-1.amazonaws.com/

## Instructions
Basic instructions for the installation and execution of the application.

### Installation
1. Install dependencies: `yarn install`.
2. Run application in development mode: `ember s`.
3. Open the application in your browser: `local server URL: http://localhost:4200/`
